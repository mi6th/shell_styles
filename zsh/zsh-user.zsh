# ZSH prompts colors

autoload -U colors && colors
PROMPT="
%{$fg_bold[blue]%}+%{$reset_color%} %{$fg_no_bold[cyan]%}%M%{$reset_color%}: %{$fg_bold[yellow]%}%~%{$reset_color%} [%*]
%{$fg_no_bold[green]%}%n%{$reset_color%} %{$fg_bold[green]%}$%{$reset_color%} "
RPROMPT="[%{$fg_bold[magenta]%}%?%{$reset_color%}]"
